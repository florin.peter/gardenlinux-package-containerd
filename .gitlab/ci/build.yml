.build:
  stage: build
  image: debian:${BUILD_DIST}-slim

  variables:
    BUILD_DIST: bullseye
    SOURCES_LIST: |
      deb http://snapshot.debian.org/archive/debian/20211012T204214Z/ bookworm main
      deb http://snapshot.debian.org/archive/debian/20211028T151025Z/ bookworm main
    OPENSSL_DIST: experimental
    GOLANG_DIST: sid
    OPENSSL_SOURCE_LIST: |
      deb http://deb.debian.org/debian ${OPENSSL_DIST} main
    GOLANG_SOURCE_LIST: |
      deb http://deb.debian.org/debian ${GOLANG_DIST} main
    OPENSSL_PREFERENCES: |
      Package: openssl:* libssl3:* libcrypto3-udeb:* libssl3-udeb:* libssl-dev:* libssl-doc:*
      Pin: release a=${OPENSSL_DIST}
      Pin-Priority: 900
    GOLANG_PREFERENCES: |
      Package: golang-github-containerd-cgroups-dev:* golang-github-containerd-continuity-dev:* golang-github-containerd-ttrpc-dev:* golang-github-containerd-typeurl-dev:* golang-github-docker-go-metrics-dev:* golang-github-gogo-googleapis-dev:* golang-github-gogo-protobuf-dev:* golang-github-grpc-ecosystem-go-grpc-prometheus-dev:* golang-github-opencontainers-image-spec-dev:* golang-github-opencontainers-runc-dev:* golang-github-prometheus-client-golang-dev:* golang-google-grpc-dev:*
      Pin: release a=bookworm
      Pin-Priority: 900
  before_script:
  - >
    if [[ $CI_DISPOSABLE_ENVIRONMENT ]]; then
      echo -n "$OPENSSL_SOURCE_LIST" > /etc/apt/sources.list.d/openssl.list
      echo -n "$GOLANG_SOURCE_LIST" > /etc/apt/sources.list.d/golang.list
      echo -n "$OPENSSL_PREFERENCES" > /etc/apt/preferences.d/openssl
      echo -n "$GOLANG_PREFERENCES" > /etc/apt/preferences.d/golang

      if [[ $OPENSSL_DIST != $BUILD_DIST ]]; then
        printf "%s\n" \
          "Package: *" \
          "Pin: release a=${OPENSSL_DIST}" \
          "Pin-Priority: 100" \
        > /etc/apt/preferences.d/openssl-exclude
      fi
      if [[ $GOLANG_DIST != $BUILD_DIST ]]; then
        printf "%s\n" \
          "Package: *" \
          "Pin: release a=${GOLANG_DIST}" \
          "Pin-Priority: 100" \
        > /etc/apt/preferences.d/golang-exclude
      fi
      printf "%s\n" \
        "Package: libc6:* libc-bin:* libc6-dev" \
        "Pin: release a=bookworm" \
        "Pin-Priority: 900" \
      > /etc/apt/preferences.d/libc-fix
      printf "%s\n" \
        "Package: libc6:* libc-bin:* libc6-dev" \
        "Pin: release a=sid" \
        "Pin-Priority: -1" \
      > /etc/apt/preferences.d/libc-dissalow-sid
    fi
  - >
    if [[ $CI_DISPOSABLE_ENVIRONMENT ]]; then
      echo -n "$SOURCES_LIST" > /etc/apt/sources.list
      if [[ $JOB_HOST_ARCH != all ]]; then
        dpkg --add-architecture $JOB_HOST_ARCH
      fi
      apt-get update -o Acquire::Check-Valid-Until=false -qy
      apt-get upgrade -qy -o DPkg::Options::=--force-unsafe-io fakeroot
      apt-get install -qy --no-install-recommends  wget ca-certificates
      mkdir temp
      cd temp
      wget -O golang-1.17-go.deb https://gitlab.com/gardenlinux/legacy-packages/garden-linux-packages-golang-1.17/-/jobs/2419196812/artifacts/raw/_output/golang-1.17-go_1.17.9-1~bpo11+1gardenlinux~0.532237134.7f197916_amd64.deb
      wget -O golang-1.17_all.deb https://gitlab.com/gardenlinux/legacy-packages/garden-linux-packages-golang-1.17/-/jobs/2419196810/artifacts/raw/_output/golang-1.17_1.17.9-1~bpo11+1gardenlinux~0.532237134.7f197916_all.deb
      wget -O golang-1.17-src.deb https://gitlab.com/gardenlinux/legacy-packages/garden-linux-packages-golang-1.17/-/jobs/2419196810/artifacts/raw/_output/golang-1.17-src_1.17.9-1~bpo11+1gardenlinux~0.532237134.7f197916_all.deb
      wget -O golang-1.17-doc.deb https://gitlab.com/gardenlinux/legacy-packages/garden-linux-packages-golang-1.17/-/jobs/2419196810/artifacts/raw/_output/golang-1.17-doc_1.17.9-1~bpo11+1gardenlinux~0.532237134.7f197916_all.deb
      dpkg -i *.deb
      cd ..
      if [[ $JOB_HOST_ARCH = all ]]; then
        apt-get build-dep -qy --indep-only -o DPkg::Options::=--force-unsafe-io ./_output/*.dsc
      else
        if [[ $JOB_HOST_ARCH != $(dpkg --print-architecture) ]]; then
          export DEB_BUILD_PROFILES="${DEB_BUILD_PROFILES:-} cross"
        fi
        apt-get build-dep -qy -a $JOB_HOST_ARCH --arch-only -o DPkg::Options::=--force-unsafe-io ./_output/*.dsc
        # Workaround for non-multiarch build-essential, see https://bugs.debian.org/666743
        apt-get install -qy --no-install-recommends binutils-$JOB_HOST_GNU_TYPE_PACKAGE gcc-$JOB_HOST_GNU_TYPE_PACKAGE g++-$JOB_HOST_GNU_TYPE_PACKAGE libc6-dev:$JOB_HOST_ARCH
      fi
    fi
  - cd _output
  - dpkg-source -x *.dsc src
  - chown nobody -R .
  - cd src
  - PATH="$PATH:/usr/lib/go-1.17/bin/"

  script:
  - >
    if [[ $JOB_HOST_ARCH != $(dpkg --print-architecture) ]]; then
      export DEB_BUILD_OPTIONS="${DEB_BUILD_OPTIONS:-} nocheck"
      export DEB_BUILD_PROFILES="${DEB_BUILD_PROFILES:-} cross"
    fi
  - su -s /bin/sh -c "set -euE; dpkg-buildpackage -B -a $JOB_HOST_ARCH" nobody

  dependencies:
  - source
  artifacts:
    paths:
    - _output/*_${JOB_HOST_ARCH}.*
    expire_in: 1 week

build all:
  extends: .build
  script:
  - su -s /bin/sh -c "set -euE; dpkg-buildpackage -A" nobody
  variables:
    JOB_HOST_ARCH: all
  rules:
  - if: '$BUILD_ARCH_ALL != ""'

build amd64:
  extends: .build
  variables:
    JOB_HOST_ARCH: amd64
    JOB_HOST_GNU_TYPE_PACKAGE: x86-64-linux-gnu
  tags:
  - gardenlinux-build-amd64
  rules:
  - if: '$BUILD_ARCH_AMD64 != ""'

build arm64:
  extends: .build
  variables:
    JOB_HOST_ARCH: arm64
    JOB_HOST_GNU_TYPE_PACKAGE: aarch64-linux-gnu
  tags:
  - gardenlinux-build-arm64
  rules:
  - if: '$BUILD_ARCH_ARM64 != ""'
